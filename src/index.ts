//#region Public Interfaces

export interface IArrayPredicateOptions {

	/**
	 * If available, start (whatever operation) at this index in the array
	 */
	start?: number;
	/**
	 * If true, perform all comparison between the string representation of objects
	 */
	string?: boolean;
	/**
	 * if true, multiple keys in predictes that try to find property values, are handled as OR
	 * so one match is enough
	 */
	any?: boolean;
	/**
	 * If true, invert the result of the predicate, so matches are those for which the predicate returns a falsy result
	 */
	invert?: boolean;
}

export interface IArraySortOptions extends IArrayPredicateOptions {
	mode?: "down" | "up";
	ignoreCase?: boolean;
}

interface IArrayReplaceOptions extends IArrayPredicateOptions {
	/**
	 * If true, add to the array if a matching object is not found
	 */
	add?: boolean;
}

export type QuerrayPredicate<T> = (t: T, index?: number, array?: Array<T>) => boolean;

type QuerrayQueryParam<T> = QuerrayPredicate<T> | { [key: string]: any, [key: number]: any } | T;

export interface IQuerray<T> {

	readonly array: Array<T>;

	readonly length: number;

	/**
	 * returns the index of the first member that satisfies the predicate
	 */
	firstIndexOf(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): number;
	first(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): T;
	/**
	 * returns a random member of the array
	 */
	random(): T;
	last(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): T;
	where(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): IQuerray<T>;
	except(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): IQuerray<T>;
	/**
	 * removes all occurances IN PLACE of elements matching the predicate
	 * @type {Function|*}
	 */
	removeAll(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): IQuerray<T>;

	/**
	 * Sorts the array using the specified field
	 * @param field the name of the field to sort on.
	 * @param {IArraySortOptions} options
	 */
	sortOn(field: string, options?: IArraySortOptions): IQuerray<T>;
	/**
	 * Returns a map of the form field: array element instance
	 */
	toMap<TTarget = T>(field: string | ((t: T) => string), transform?: (v: T) => TTarget): Record<string, TTarget>;
	/**
	 * returns a copy of the array with no identical members.
	 * @param { any } predicate If provided, this predicate will be used to determine element equality
	 * @param { any } options Optional configuration for the predicate
	 */
	//unique(predicate?: any, options?: IArrayPredicateOptions ): IQuerray<T>;
	selectProperty<R>(field: string): IQuerray<R>;
	replace(predicate: QuerrayQueryParam<T>, newElement: T, options?: IArrayReplaceOptions): IQuerray<T>;
	clear(): IQuerray<T>;
	/**
	 * Shuffles the array inplace
	 * */
	shuffle(): IQuerray<T>;
	/**
	 * returns a random element
	 */
	random(): T;
	/**
	 * returns a random valid index or -1 if the array has no elements
	 */
	randomIndex(): number;
	/**
	 * Counts the elements matching a predicate
	 */
	count(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): number;
	/**
	 * Exchange the elements at indices ind1, ind2
	 */
	exchange(ind1: number, ind2: number): IQuerray<T>;
	/**
	 * Moves an item from position <strong>from</strong> to position <strong>to</strong>
	 * The TO position refers to the array BEFORE the move
	 */
	move(from: number, to: number): IQuerray<T>;
	/**
	 * like splice (without the insertion part), returns the array rather than the spliced part
	 * @param at the index to start removing at
	 * @param howMany how many elements to remove (defaults to 1)
	 */
	removeAt(at: number, howMany?: number): IQuerray<T>;

	/**
	 * like splice (without the deletion part), returns the array
	 * @param item 
	 * @param at the index to start removing at
	 */
	insertAt(at: number, ...args: T[]): IQuerray<T>;

	///// Overrides for array

	filter(func: (t: T, i?: number, arr?: T[]) => any, thisArg?: any): IQuerray<T>;


}

export interface IQItem<T> {
	readonly index: number;
	readonly item: T;
}

export interface IQIterator<T> {
	readonly q: IQuerray<T>;
	readonly isCyclical: boolean;
	next(): IQItem<T> | null;
	prev(): IQItem<T> | null;
	goto(ind: number): IQItem<T> | null;
	other(): IQItem<T> | null;
	current(): IQItem<T>;
}

interface IQuerrayConstructor {
	new <T>(array: T[]): IQuerray<T>;
}

//#endregion

function getValueByKeys(obj: any, keys: string | Array<string>) {
	let len: number;
	if (typeof keys === "string") {
		keys = keys.split('.');
	}
	if (!obj || !keys || !(len = keys.length)) {
		return null;
	}
	if (len === 1) {
		return obj[keys[0]];
	}
	--len; // iterate up to last component
	for (let i = 0; i < len; ++i) {
		obj = obj[keys[i]];
		if (!obj) {
			return obj === undefined ? null : obj;
		}
	}
	return obj[keys[len]];
}

/**
 * The object may contain a field like { name: /^T/ }, indicating that the name should match the regexp
 * @param data 
 * @param options 
 */
function _toPredicate<T>(data: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): QuerrayPredicate<T> {
	const t = typeof data;
	let asString: boolean,
		ret: QuerrayPredicate<T>;
	options = options || {};
	asString = Boolean(options.string);

	if ("function" === t) {
		ret = data as QuerrayPredicate<T>;
	}
	else if ("object" === t) {
		if (data instanceof RegExp) {
			ret = function (e) {
				return (data as RegExp).test(String(e));
			};
		}
		else {
			const any = options.any,
				keys = (Object as any).keys(data).map(key => ({
					rawKey: key,
					splitKey: String(key).split('.')
				})),
				len = keys.length;
			ret = function findByKeys(e: T) {
				if (keys.length === 0) { // fail on an empty search
					return false;
				}
				let i: number, key: { rawKey: string, splitKey: Array<string> }, equal,
					keyData,
					eData,
					result = true;
				for (i = 0; i < len; ++i) {
					key = keys[i];
					eData = getValueByKeys(e, key.splitKey);
					keyData = data[key.rawKey];
					if (asString) {
						eData = String(eData);
						keyData = String(keyData);
					}
					if (keyData instanceof RegExp) {
						equal = (typeof eData === "string") && data[key.rawKey].test(eData);
					}
					else {
						equal = eData === keyData;
					}
					if (equal) {
						if (any) {
							return true;
						}
					}
					else { // not equal
						if (any) {
							result = false;
						}
						else {
							return false;
						}
					}
				}
				return result;
			};
		}
	}
	else { // not an object, not a function
		if (asString) {
			const sdata = String(data);
			ret = function findLiteralString(e) {
				return String(e) === sdata;
			};
		}
		else {
			ret = function findLiteral(e) {
				return e === data;
			};
		}
	}
	if (options.invert) {
		var orig = ret;
		ret = function (e) {
			return !orig(e);
		};
	}
	return ret;
}


/**
 * This function calls itself if the resulting array is the same as the original one. 
 * @param arr 
 * @param copyOfArr for internal use
 * @param count for internal use, number of recursive calls, so we bail out at some point
 */
function shuffle<T>(arr: Array<T>, copyOfArr: Array<any> = null, count: number = 0): Array<T> {
	const len = arr.length;
	if (len < 2 || count > 5) {
		return arr;
	}
	if (!copyOfArr) {
		copyOfArr = arr.slice();
	}
	let j: number,
		temp: any;
	for (let i = arr.length - 1; i > 0; i--) {
		j = Math.floor(Math.random() * i);
		if (j !== i) {
			temp = arr[i];
			arr[i] = arr[j];
			arr[j] = temp;
		}
	}
	for (let i = arr.length - 1; i > 0; i--) { // no need to check the all elements, just len-1
		if (arr[i] !== copyOfArr[i]) {
			// if the arrays are not identical
			return arr;
		}
	}
	// the arrays were identical, reshuffle
	return shuffle(arr, copyOfArr, count + 1);
}

class QIterator<T> implements IQIterator<T> {
	private _index: number;
	constructor(public readonly q: IQuerray<T>, public readonly isCyclical: boolean) {
		this._index = 0;
	}

	public next(): IQItem<T> | null {
		return this._move(1);
	}

	public prev(): IQItem<T> | null {
		return this._move(-1);
	}

	public goto(index: number): IQItem<T> | null {
		if (!this.isValidIndex(index)) {
			return null;
		}
		return {
			index: this._index = index,
			item: this.q.array[index]
		};
	}

	public current(): IQItem<T> {
		return {
			index: this._index,
			item: this.q.array[this._index]
		}
	}

	public other(): IQItem<T> | null {
		const len = this.q.length;
		if (len < 2) {
			return null;
		}
		let otherInd = -1;
		for (let i = 0; i < 5 && otherInd < 0; ++i) {
			const ind = this.q.randomIndex();
			if (ind !== this._index) {
				otherInd = ind;
			}
		}
		if (otherInd < 0) {
			if (this.isValidIndex(this._index + 1)) {
				otherInd = this._index + 1;
			}
			else {
				otherInd = this._index - 1;
			}
		}
		return {
			index: otherInd,
			item: this.q.array[otherInd]
		}
	}

	public isValidIndex(ind: number): boolean {
		if (isNaN(ind)) {
			return false;
		}
		const rnd = Math.round(ind);
		if (rnd !== ind) {
			return false;
		}
		return rnd >= 0 && rnd < this.q.length;
	}

	private _move(inc: number): IQItem<T> | null {
		let ind = this._index + inc;

		if (ind < 0 || ind >= this.q.length) {
			if (!this.isCyclical) {
				return null;
			}
			ind = inc > 0 ? 0 : this.q.length - 1;
		}
		this._index = ind;
		return {
			index: ind,
			item: this.q.array[ind]
		}
	}
}

export interface ITestIt {
	readonly me: number;
}
export class Querray<T> implements IQuerray<T> {
	private readonly _array: Array<T>;

	public constructor(array: Array<T>) {
		this._array = array || [];
	}

	public gett(): ITestIt {
		return {
			me: 0
		}
	}

	public getIterator(cyclical: boolean): IQIterator<T> {
		return new QIterator<T>(this, cyclical);
	}

	public otherIndex(ind: number): number {
		const it = this.getIterator(false);
		if (it.goto(ind)) {
			const ot = it.other();
			return ot?.index ?? -1;
		}
		return -1;
	}

	public clone(): Querray<T> {
		return new Querray(this._array.slice());
	}

	public toString(): string {
		return String(this._array);
	}

	public get array(): Array<T> {
		return this._array;
	}

	public set length(len: number) {
		if (len >= 0) {
			this._array.length = Math.round(len);
		}
	}

	public get length(): number {
		return this._array.length;
	}

	public remove(e: T): Querray<T> {
		if (!this) {
			throw new TypeError();
		}
		const ind = this._array.indexOf(e);
		if (ind >= 0) {
			this._array.splice(ind, 1);
		}
		return this;
	}

	public push(...args: T[]): number {
		if (args.length) {
			this._array.push.apply(this._array, args);
		}
		return this._array.length;
	}

	public pop(): T {
		return this._array.pop();
	}

	public slice(start?: number, end?: number): Querray<T> {
		return new Querray(this._array.slice(start, end));
	}

	public filter(func: (t: T, i?: number, arr?: T[]) => any, thisArg?: any): Querray<T> {
		return new Querray<T>(this._array.filter(func as any, thisArg));
	}

	public random(): T {
		return this._array[this.randomIndex()];
	}

	public randomIndex(): number {
		return Math.round(Math.random() * (this.length - 1));
	}

	public shuffle(): Querray<T> {
		shuffle(this._array);
		return this;
	}

	public firstIndexOf(predicate: QuerrayQueryParam<T>, options: IArrayPredicateOptions): number {
		const len = this.length;
		let element: T,
			start: number;
		const test = _toPredicate(predicate, options);
		start = (options && options.start) || 0;
		for (let i = start; i < len; ++i) {
			element = this._array[i];
			if (test(element)) {
				return i;
			}
		}

		return -1;
	}


	/**
	 * Replace all elements matching predicate, with newElement
	 * @param predicate 
	 * @param newElement 
	 * @param options 
	 */
	public replace(predicate: QuerrayQueryParam<T>, newElement: T, options?: IArrayReplaceOptions): this {
		const arr = this._array,
			len = arr.length;
		let element: T,
			found = false,
			add = options && options.add;
		if (len === 0) {
			if (add) {
				arr.push(newElement);
			}
			return this;
		}
		const test = _toPredicate(predicate, options);
		for (let i = 0; i < len; ++i) {
			element = arr[i];
			if (test(element)) {
				found = true;
				arr[i] = newElement;
			}
		}
		if (add && !found) {
			arr.push(newElement);
		}

		return this;
	}

	public indexFromEnd(predicate, options: IArrayPredicateOptions): number {
		const arr = this._array;
		const len = arr.length;
		let element: T;
		if (len === 0) {
			return -1;
		}
		const test = _toPredicate(predicate, options);
		for (let i = len - 1; i >= 0; --i) {
			element = arr[i];
			if (test(element)) {
				return i;
			}
		}

		return -1;
	}

	public last(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): T {
		var ind = this.indexFromEnd(predicate, options);
		return ind >= 0 ? this._array[ind] : null;
	}

	public first(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): T {
		var ind = this.firstIndexOf(predicate, options);
		if (ind >= 0) {
			return this._array[ind];
		}

		return null;
	}

	public where(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): Querray<T> {
		const test = _toPredicate(predicate, options);
		return this.filter(test);
	}

	public except(predicate: QuerrayQueryParam<T>, options: IArrayPredicateOptions): Querray<T> {
		if (!this) {
			throw new TypeError();
		}

		options = (Object as any).assign({}, options, { invert: true });
		return this.where(predicate, options);
	}

	public exchange(ind1: number, ind2: number): Querray<T> {
		if (ind1 === ind2) {
			return this;
		}
		const arr = this._array;
		const len = arr.length;
		if (ind1 < 0 || ind2 < 0 || ind1 >= len || ind2 >= len) {
			return this;
		}
		let temp = arr[ind1];
		arr[ind1] = arr[ind2];
		arr[ind2] = temp;
		return this;
	}

	public move(from: number, to: number): Querray<T> {
		const arr = this._array;
		const len = arr.length;
		if (isNaN(from) || isNaN(to) || from < 0 || from >= len) {
			return this;
		}

		if (to < 0) {
			to = 0;
		}
		if (to >= len) {
			to = len - 1;
		}
		if (from === to) {
			return this;
		}
		let temp = arr[from];
		arr.splice(from, 1);
		arr.splice(to, 0, temp);
		return this;
	}

	public removeAll(predicate: QuerrayQueryParam<T>, options: IArrayPredicateOptions): Querray<T> {
		const arr = this._array;
		const len = arr.length;
		if (!len) {
			return this;
		}
		const test = _toPredicate(predicate, options);
		for (let i = len - 1; i >= 0; --i) {
			if (test(arr[i], i, arr)) {
				arr.splice(i, 1);
			}
		}
		return this;
	}

	public count(predicate: QuerrayQueryParam<T>, options?: IArrayPredicateOptions): number {
		const arr = this._array;
		const len = arr.length;
		if (len === 0) {
			return 0;
		}
		const test = _toPredicate(predicate, options);
		let ret = 0;
		let ind = len;
		while (ind-- > 0) {
			if (test(arr[ind], ind, arr)) {
				ret++;
			}
		}
		return ret;
	}

	/**
	 * With no field, will produce a map with the keys equal to the string versions of the array
	 * elements. With a field, will produce a map of the value in [field] => the element
	 * @param field 
	 * @param transform 
	 */
	public toMap<TTarget = T>(field: string | ((t: T) => string), transform?: (v: T) => TTarget): Record<string, TTarget> {
		const arr = this._array;
		const len = arr.length;
		if (len === 0) {
			return {};
		}
		const ret: Record<string, TTarget> = {};
		let key: any,
			c: T;
		const getKey = typeof field === "string" ? (r: T) => r[field] : field;
		for (let i = 0; i < len; ++i) {
			c = arr[i];
			if (c) {
				key = getKey(c);
				ret[String(key)] = transform ? transform(c) : c as unknown as TTarget;
			}
		}
		return ret;
	}

	public selectProperty<R>(field: string): Querray<R> {
		const arr = this._array;
		const len = arr.length;
		if (len === 0) {
			return new Querray<R>(null);
		}

		if (!field) {
			throw new Error("selectProperty: missing field");
		}
		const ret: Array<R> = [];
		const fieldParts = field.split('.');
		let c: T;
		for (let i = 0; i < len; ++i) {
			c = arr[i];
			if (!c) {
				ret[i] = null;
			}
			else {
				ret[i] = getValueByKeys(c, fieldParts);
			}
		}
		return new Querray(ret);
	}

	public sortOn(field: string, options: IArraySortOptions): Querray<T> {
		const arr = this._array;
		const len = arr.length;
		if (len === 0) {
			return this;
		}
		options = options || {};
		const invertFactor = options.mode === "down" ? -1 : 1,
			ignoreCase = Boolean(options.ignoreCase),
			getItem = ignoreCase ?
				function (s) {
					return typeof s === "string" ?
						s.toLowerCase()
						: s;
				}
				: function (a: any) { return a; };
		const fieldParts = field.split('.');
		arr.sort(
			function (a: T, b: T) {
				const f1: any = getItem(getValueByKeys(a, fieldParts)),
					f2: any = getItem(getValueByKeys(b, fieldParts));
				return invertFactor * (Number(f1 > f2) - Number(f1 < f2));
			}
		);
		return this;
	}

	public clear(): Querray<T> {
		const arr = this._array;
		const len = arr.length;
		len && arr.splice(0, len);
		return this;
	}

	public insertAt(at: number, ...items: T[]) {
		const arr = this._array;
		const len = arr.length;
		if (len === 0 || at < 0 || at >= len || items.length < 1) {
			return this;
		}
		for (let i = 0; i < items.length; ++i) {
			arr.splice(at + i, 0, items[i]);
		}
		return this;
	}

	public removeAt(at: number, howMany = 1) {
		const arr = this._array;
		const len = arr.length;
		if (len === 0 || at < 0 || at >= len) {
			return this;
		}
		if (at + howMany > len) {
			howMany = len - at;
		}
		arr.splice(at, howMany);
		return this;
	}
}
