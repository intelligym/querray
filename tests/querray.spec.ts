import { test, expect } from '@playwright/test';
import { Querray } from "../src/index";

test.describe("Querray Tests", () => {

	test("Core functions", async ({ page }) => {
		const recs = new Querray(pplArray);
		console.log("impostor: ", recs.first({ name: "impostor" }));
		recs.removeAt(2);
		recs.shuffle();
		recs.insertAt(1, { name: "new guy", id: 444, details: { class: "new" } }, { name: "new zere", id: 555, details: { class: "new" } },
			{ name: "new gal", id: 644, details: { class: "officer" } }, { name: "new impostor", id: 777, details: { class: "worker" } });
		for (let i = 0; i < 3; ++i) {
			console.log("random elements: ", recs.random());
		}
		console.log("selectProperty name", recs.selectProperty("name"));
		console.log("selectProperty details.class", recs.selectProperty("details.class"));
		console.log("selectProperty details.more", recs.selectProperty("details.more"));
		console.log("filtered selectProperty details.more", recs.selectProperty("details.more").filter(Boolean));

		console.log("Before exchange", recs.array[0].name, recs.array[3].name);
		recs.exchange(0, 3);
		console.log("After exchange 0, 3", recs.array[0].name, recs.array[3].name);

		console.log("Before exchange", recs.array[6].name, recs.array[2].name);
		recs.exchange(6, 2);
		console.log("After exchange 6, 2", recs.array[6].name, recs.array[2].name);

		console.log("Item 3 Before move ", recs.array[3].name);
		recs.move(3, 5);
		console.log("Item 3 after move ", recs.array[3].name);

		recs.move(4, 1);

		recs.move(recs.length, 2);

		console.log("impostor: ", recs.first({ name: "impostor" }));
		console.log("new impostor: ", recs.first({ name: "new impostor" }));
	});

	test("Iterators", async () => {
		const arr = randomArray(),
			clone = arr.slice(),
			it = new Querray(arr).getIterator(true);
		it.q.shuffle();
		expect(eq(arr, clone), "Arrays should not be equal after shuffling").toBe(false);
		expect(it.goto(arr.length - 1), "Failed to set iterator to last place").toBeTruthy();
		expect(it.current().index, "goto failed to set index").toEqual(arr.length - 1);
		expect(it.next(), "cyclical next should work").toBeTruthy();
		expect(it.current().index, "cyclical next didn't move to 0").toEqual(0);

		const it1 = new Querray(arr).getIterator(false);

	});
});

interface Rec {
	readonly name: string;
	readonly id: number;
	readonly details: any;
}


const pplArray: Rec[] = [
	{
		name: "john",
		id: 2446,
		details: { class: "worker" }
	},
	{
		name: "jack",
		id: 24446,
		details: { class: "worker" }
	},
	{
		name: "impostor",
		id: 8446,
		details: { class: "impostor" }
	},
	{
		name: "grid",
		id: 442446,
		details: { class: "officer" }
	},
	{
		name: "tayeb",
		id: 668746,
		details: { class: "striker" }
	},
	{
		name: "rabak",
		id: 454446,
		details: { class: "officer", more: "some" }
	},
	{
		name: "kind",
		id: 52446,
		details: { class: "worker" }
	},
	{
		name: "anbo",
		id: 8246,
		details: { class: "striker" }
	},
	{
		name: "coby",
		id: 92446,
		details: { class: "officer" }
	}];


const randomArray = () => {
	const ret = new Array();
	for (let len = 0; len < 100; ++len) {
		ret[len] = String.fromCharCode(65 + Math.round(Math.random() * 27))
	}
	return ret;
}

const eq = (arr1: Array<any>, arr2: Array<any>) => {
	if (arr1?.length !== arr2?.length) {
		return false;
	}
	const ind = arr1.findIndex((value, ind) => {
		return arr2[ind] !== value;
	})
	return ind === -1;
}
